package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("tva")
public class TvaRessource {
	 
	private CalculTva calculTva = new CalculTva();
	 @GET
	 @Path("tauxpardefaut")
	 public double getValeurTauxParDefaut() {
	         return TauxTva.NORMAL.taux;
	 }
	 
	 @GET
	 @Path("valeur/{niveauTva}")
	 public double getValeurTaux(@PathParam("niveauTva") String niveau) {
		 try {
	            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
	        }
	        catch ( Exception ex ) {
	            throw new NiveauTvaInexistantException();
	        }
	 }

	 @GET
	 @Path("{valeur}")
	 public double somme(@PathParam("valeur") String value, @QueryParam("somme") double nb) {
		 TauxTva taux = TauxTva.fromString(value.toUpperCase());
		 try {
			 return new CalculTva().calculerMontant(taux, nb);
		 }
	        catch ( Exception ex ) {
	            throw new NiveauTvaInexistantException();
	        }
	 }
	 @GET
	 @Path("lestaux")
	 @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	 public List<InfoTauxDto> getInfoTaux() {
	   ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
	   for ( TauxTva t : TauxTva.values() ) {
	     result.add(new InfoTauxDto(t.name(), t.taux));
	   }
	   return result;
	 }

	 @GET
	    @Path("details/{taux}")
	    @Produces({MediaType.APPLICATION_JSON})
	    public List<InfoTauxDto> getDetail(@PathParam("taux") String taux, @QueryParam("somme") int somme) {
	    	ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
	    	result.add(new InfoTauxDto("montantTotal",new CalculTva().calculerMontant(TauxTva.valueOf(taux.toUpperCase()), somme)));
	    	result.add(new InfoTauxDto("montantTva", TauxTva.valueOf(taux.toUpperCase()).taux ));
	    	result.add(new InfoTauxDto("somme", somme));
	    	return result;
	    }
}
